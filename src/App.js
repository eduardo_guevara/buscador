import React, { Component } from 'react';
import Buscador from './components/Buscador';
import Resultado from './components/Resultado';


//import { render } from '@testing-library/react';



class App extends Component {

  //Es un objeto (Se puede ver dentro de un constructor) Sirve para guardar el estado
 state = {
   termino : '',
   imagenes : [],
   pagina:''
 }

 scroll = () => {
   const elemento = document.querySelector('.jumbotron');
   elemento.scrollIntoView('smooth', 'start');
 }

 paginaAnterior = () => {
   //leer el state de la pagina actual
   let pagina = this.state.pagina;
   //leer si la pagina es 1, ya no ir hacia atras
   if(pagina === 1)  return null;
   //Sumar uno a la pagina actual
    pagina -=1;
   //Agregar el cambio al state
    this.setState({
      pagina
    }, ()=>  {
      this.consultarApi();
      this.scroll();
    });
  console.log('Anterior..'+ pagina);
 }

 paginaSiguiente = () => {
   //leer el state de la pagina actual
    let pagina = this.state.pagina;
   //Sumar uno a la pagina actual
    pagina ++;
   //Agregar el cambio al state
    this.setState({
      pagina
    }, ()=>  {
      this.consultarApi();
      this.scroll();
    });
  console.log('Siguiente..'+ pagina);
}

//Consumir la ApiRest
consultarApi = () =>{

const termino = this.state.termino;  
const pagina = this.state.pagina;

const url =`https://pixabay.com/api/?key=1732750-d45b5378879d1e877cd1d35a6&q=${termino}&per_page=20&page=${pagina}`;

console.log(url);

fetch(url)
.then(respuesta => respuesta.json () )
.then(resultado => this.setState({ imagenes : resultado.hits } ) )
//console.log("imagenes:"+imagenes);
}




//Funcion en la que se anida(modificacion de state y consultar api). Al realizar la busqueda hace el
//callback a la funcion consultar api
  datosBusqueda = (termino) => {
  
    this.setState({
      termino}, () => {
        this.consultarApi();
      })
    console.log(termino);
  }

  render() {
  return (
    <div className="app container">
      <div className="jumbotron">
        <p className="lead text-center">Buscador de imágenes</p>
        <Buscador 
        datosBusqueda = {this.datosBusqueda}
        />   
      </div> 
      <div className="row justify-content-center">
        <Resultado
          imagenes={this.state.imagenes}
          paginaAnterior={this.paginaAnterior}
          paginaSiguiente={this.paginaSiguiente}
        /> 
      </div>   
    </div>
  );
}
}

export default App;
