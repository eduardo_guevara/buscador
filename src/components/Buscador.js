import React, { Component } from 'react';

class Buscador extends Component {
    //Declaracion para coger los datos
    busquedaRef = React.createRef();

    //Funcion de coger datos
    handle = (e) =>    {
        //Obligatorio para obtener datos
        e.preventDefault();
        //Tomamos el valor del input
        const termino = this.busquedaRef.current.value;
        //Lo enviamos al componente principal
        this.props.datosBusqueda(termino);

        console.log(this.busquedaRef.current.value);
    }

    render(){
        return(
            <form onSubmit={this.handle}>
                <div className="row">
                    <div className="form-group col-md-8">
                        <input ref={this.busquedaRef} type="text" className="form-control form-control-lg"
                        placeholder="Busca tu imagen. Ejemplo Fútbol" />
                    </div>
                    <div className="form-group col-md-4">
                        <input type="submit" className="btn btn-lg btn-danger btn-block"
                        value='Buscar'/>
                    </div>
                </div>
            </form>
    
        );

    }
}

export default Buscador;